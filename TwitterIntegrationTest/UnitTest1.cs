﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using TweetSharp;
using Services;
using Moq;
using NUnit.Framework;

namespace TwitterIntegrationTest
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void If_TwitterConsumerAndSecretExist__Then_DoNotThrowError()
        {
            TwitterClientInfo connectionInfo = new TwitterClientInfo
            {
                ConsumerKey = "test-consumer-key",
                ConsumerSecret = "test-consumer-secret"
            };
            
            Assert.That(() => new TwitterConnectionService(connectionInfo), Throws.Nothing);
        }

        [Test]
        public void If_TwitterConsumerOrSecretDoesNotExist__Then_ThrowException()
        {
            TwitterClientInfo connectionInfo = new TwitterClientInfo
            {
                ConsumerSecret = "test-consumer-secret"
            };

            Assert.Throws<InvalidOperationException>(() => new TwitterConnectionService(connectionInfo));
        }

        [Test]
        public void If_TwitterUserIdIsNotAvailable__Then_ThrowException()
        {
            TwitterClientInfo connectionInfo = new TwitterClientInfo
            {
                ConsumerKey = "Uudmr6M3AjnTvfIOZge97F3z7",
                ConsumerSecret = "x08SOQNGuvwriGLpgWzCVpBKL0ocvvxVk9J6ckbjTyQ3tLET0S"
            };
            TwitterConnectionService _twitterService = new TwitterConnectionService(connectionInfo);

            GetUserProfileForOptions queryInfo = new GetUserProfileForOptions
            {
                IncludeEntities = true,
                ScreenName = "LukobaDaniel"
            };
            _twitterService.FetchUserInfo(queryInfo);

            Assert.Throws<InvalidOperationException>(() => _twitterService.FetchUserInfo(queryInfo));
        }

        public void If_TwitterUserIdIsAvailable__Then_ReturnTwitterUserInfo()
        {
            TwitterClientInfo connectionInfo = new TwitterClientInfo
            {
                ConsumerKey = "Uudmr6M3AjnTvfIOZge97F3z7",
                ConsumerSecret = "x08SOQNGuvwriGLpgWzCVpBKL0ocvvxVk9J6ckbjTyQ3tLET0S"
            };            
            GetUserProfileForOptions queryInfo = new GetUserProfileForOptions
            {
                IncludeEntities = true,
                ScreenName = "LukobaDaniel",
                UserId = 873924765060395008
            };
            var userData = new TwitterUser()
            {
                CreatedDate = new DateTime(2017, 08, 01, 23, 00, 00),
                FavouritesCount = 0,
                FollowersCount = 2,
                FriendsCount = 2,
                Id = queryInfo.UserId.Value,
                IsProfileBackgroundTiled = false,
                ListedCount = 2,
                Name = queryInfo.ScreenName,
                StatusesCount = 0,
                Status = new TwitterStatus
                {

                }
            };

            var mock = new Mock<TwitterConnectionService>(connectionInfo);
            mock.Setup(x => x.FetchUserInfo(queryInfo))
                .Returns(userData);
            
            Assert.AreEqual(queryInfo.UserId, userData.Id);
        }
    }
}
