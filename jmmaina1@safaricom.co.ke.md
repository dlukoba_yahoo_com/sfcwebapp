# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Design notes ###

* [Apiary documentation](http://private-7d4e5-sfctwitterapidocumentation.apiary-mock.com)
+ Handling over 1M user requests
	* [Use case showing scalable implementation](https://bitbucket.org/dlukoba_yahoo_com/sfcwebapp/src/5e8c51ceecd75c0cb3c1be24dceae8a25ae51abf/Docs/scalable_user_request_handling.png?at=development&fileviewer=file-view-default)
	* Using a message queue, for example, rabbitmq to handle inbound requests
	* If rate limiting is used, a user's status can be retrieved from the cache to speed it up
	* The cache can be partitioned based on a hash of the username
	* Spin up multiple instances to handle requests for various partitions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines