﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TweetSharp;
using Serilog;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;

namespace Services
{
    public class TwitterConnectionService
    {
        TwitterClientInfo _clientInfo;
        TwitterService _twitterSvc;
        ILogger _logger = Log.ForContext<TwitterConnectionService>();
        TelemetryClient _telemetryClient;

        public TwitterConnectionService(TwitterClientInfo clientInfo)
        {
            if (string.IsNullOrEmpty(clientInfo.ConsumerKey) || 
                string.IsNullOrEmpty(clientInfo.ConsumerSecret))
            {
                _logger.Error("invalid connection information provided");
                throw new InvalidOperationException("Client information is not valid");                
            }

            _clientInfo = clientInfo;
            _twitterSvc = new TwitterService(_clientInfo);
            _telemetryClient = new TelemetryClient();
        }

        /// <summary>
        /// retrieve a user's information from twitter based on a user's id
        /// </summary>
        /// <param name="userInfo"></param>
        public TwitterUser FetchUserInfo(GetUserProfileForOptions userInfo)
        {
            if (!userInfo.UserId.HasValue)
            {
                _logger.Error("cannot fetch information since user id is missing");
                throw new InvalidOperationException("The user id is missing");
            }
            TwitterUser user = null;
            
            try
            {
                //log valid user request for user X
                var userRequestCounter = new MetricTelemetry();
                userRequestCounter.Name = userInfo.UserId.Value.ToString();
                userRequestCounter.Count += 1;
                _telemetryClient.TrackMetric(userRequestCounter);

                //hard coded for now
                _twitterSvc.AuthenticateWith("3292860845-i2F3pC4uJQNC5LmOl8DqGAUImOSmkMc1KIhn8z2", "Jg3f9cWrDjBtY1FHpGgjsUBmXljdfuat2pTnPMYWx5EGA");
                user = _twitterSvc.GetUserProfileFor(userInfo);
            }
            catch(Exception ex)
            {
                _logger.Error(ex, "Error fetching data for user {userid}", userInfo.UserId);
            }
            finally
            {

            }

            return user;
        }
    }
}
