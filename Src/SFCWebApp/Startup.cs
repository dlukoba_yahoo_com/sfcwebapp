﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SFCWebApp.Startup))]
namespace SFCWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
